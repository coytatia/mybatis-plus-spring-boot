package com.coytatia.core.bean;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import java.util.Map;

/**
 * FileName  : SwitchDataSource.java
 * CreateDate: 2018/7/23 22:57
 *
 * @author Wong
 */
public final class RouteDataSource extends AbstractRoutingDataSource {

    private static String defaultDatasourceKey;

    private static final ThreadLocal<String> DATASOURCE_KEY_IN_THREAD_LOCAL = new ThreadLocal<>();

    public static String getDefaultDatasourceKey() {
        return defaultDatasourceKey;
    }

    public static void destroy() {
        DATASOURCE_KEY_IN_THREAD_LOCAL.remove();
    }

    public static void switchDatasource(String datasourceKey) {
        DATASOURCE_KEY_IN_THREAD_LOCAL.set(datasourceKey);
    }

    public static String getDatasourceKey() {
        return DATASOURCE_KEY_IN_THREAD_LOCAL.get();
    }

    @Override
    protected Object determineCurrentLookupKey() {
        return getDatasourceKey();
    }

    public static RouteDataSource initRouteDataSource(Map<Object, Object> targetDataSources, Object defaultTargetDataSource, String defaultDataSourceKey) {
        RouteDataSource routeDataSource = new RouteDataSource();
        routeDataSource.setTargetDataSources(targetDataSources);
        routeDataSource.setDefaultTargetDataSource(defaultTargetDataSource);
        defaultDatasourceKey = defaultDataSourceKey;
        return routeDataSource;
    }

    private RouteDataSource() {
    }
}
