package com.coytatia.handle;

import com.coytatia.core.annotation.SwitchDataSource;
import com.coytatia.core.bean.RouteDataSource;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FileName  : MybatisDynamicRouteSwitchHandler.java
 * CreateDate: 2018/8/7 0:37
 *
 * @author Wong
 */
@Aspect
public class DynamicRouteSwitchHandler {

    private static final Logger logger = LoggerFactory.getLogger(DynamicRouteSwitchHandler.class);

    @Around(value = "@annotation(switchDataSource)")
    public Object autoRedisCache(ProceedingJoinPoint pjd, SwitchDataSource switchDataSource) throws Throwable {
        Object result;

        RouteDataSource.switchDatasource(switchDataSource.value());

        logger.info("switch datasource : " + RouteDataSource.getDatasourceKey());

        result = pjd.proceed();

        RouteDataSource.destroy();
        logger.info("execution switch datasource, switch default datasource : " + RouteDataSource.getDefaultDatasourceKey());

        return result;

    }

}
