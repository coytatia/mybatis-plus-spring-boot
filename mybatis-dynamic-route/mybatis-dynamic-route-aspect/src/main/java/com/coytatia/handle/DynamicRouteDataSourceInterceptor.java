package com.coytatia.handle;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.support.AopUtils;
import org.springframework.stereotype.Component;

/**
 * FileName  : DynamicRouteDataSourceInterceptor.java
 * CreateDate: 2018/8/7 17:19
 *
 * @author Wong
 * <p>
 * Modification  History:
 * Date       Author     Version      Discription
 * -----------------------------------------------------------------------------------
 * 2018/8/7   Wong       1.0             1.0
 * <p>
 * Why & What is modified: <修改原因描述>
 * @version V1.0
 */
//@Component
public class DynamicRouteDataSourceInterceptor extends DynamicRouteAspectSupport implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        Class<?> targetClass = (invocation.getThis() != null ? AopUtils.getTargetClass(invocation.getThis()) : null);
        return this.invokeWithinDynamicRoute(invocation.getMethod(), targetClass,invocation::proceed);
    }
}
