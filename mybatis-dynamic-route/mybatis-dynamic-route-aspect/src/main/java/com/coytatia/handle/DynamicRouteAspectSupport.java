package com.coytatia.handle;

import com.coytatia.core.annotation.SwitchDataSource;
import com.coytatia.core.bean.RouteDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;

import java.lang.reflect.Method;

/**
 * FileName  : DynamicRouteAspectSupport.java
 * CreateDate: 2018/8/7 17:32
 *
 * @author Wong
 * <p>
 * Modification  History:
 * Date       Author     Version      Discription
 * -----------------------------------------------------------------------------------
 * 2018/8/7   Wong       1.0             1.0
 * <p>
 * Why & What is modified: <修改原因描述>
 * @version V1.0
 */
public abstract class DynamicRouteAspectSupport {

    @FunctionalInterface
    protected interface InvocationCallback {

        Object proceedWithInvocation() throws Throwable;
    }

    private static final Logger logger = LoggerFactory.getLogger(DynamicRouteAspectSupport.class);

    protected Object invokeWithinDynamicRoute(Method method, @Nullable Class<?> targetClass,
                                              final DynamicRouteAspectSupport.InvocationCallback invocation) throws Throwable {

        SwitchDataSource methodSwitchDataSource = method.getAnnotation(SwitchDataSource.class);

        if (methodSwitchDataSource != null) {
            // 假如方法贴了注解 则使用方法的忽视类上的
            return invocation.proceedWithInvocation();
        }

        SwitchDataSource classSwitchDataSource = method.getDeclaringClass().getAnnotation(SwitchDataSource.class);

        Object result;

        RouteDataSource.switchDatasource(classSwitchDataSource.value());

        logger.info("switch datasource : " + RouteDataSource.getDatasourceKey());

        result = invocation.proceedWithInvocation();

        RouteDataSource.destroy();
        logger.info("execution switch datasource, switch default datasource : " + RouteDataSource.getDefaultDatasourceKey());

        return result;
    }
}
