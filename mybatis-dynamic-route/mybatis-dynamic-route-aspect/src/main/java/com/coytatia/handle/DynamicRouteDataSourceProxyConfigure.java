package com.coytatia.handle;

import com.coytatia.core.annotation.SwitchDataSource;
import org.springframework.aop.framework.autoproxy.BeanNameAutoProxyCreator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * FileName  : DynamicRouteDataSourceProxyConfigure.java
 * CreateDate: 2018/8/7 17:22
 *
 * @author Wong
 * <p>
 * Modification  History:
 * Date       Author     Version      Discription
 * -----------------------------------------------------------------------------------
 * 2018/8/7   Wong       1.0             1.0
 * <p>
 * Why & What is modified: <修改原因描述>
 * @version V1.0
 */
//@Configuration
public class DynamicRouteDataSourceProxyConfigure {

    @Bean
    public BeanNameAutoProxyCreator beanNameAutoProxyCreator(ApplicationContext context) {

        // 扫描在类上贴了@SwitchDataSource的bean
        String[] beanNamesForSwitchDataSourceAnno = context.getBeanNamesForAnnotation(SwitchDataSource.class);

        BeanNameAutoProxyCreator beanNameAutoProxyCreator = new BeanNameAutoProxyCreator();
        if (beanNamesForSwitchDataSourceAnno != null && beanNamesForSwitchDataSourceAnno.length > 0) {
            //设置要创建代理的那些Bean的名字
            beanNameAutoProxyCreator.setBeanNames(beanNamesForSwitchDataSourceAnno);

        }
        //设置拦截链名字(这些拦截器是有先后顺序的)
        beanNameAutoProxyCreator.setInterceptorNames(context.getBeanNamesForType(DynamicRouteDataSourceInterceptor.class));
        return beanNameAutoProxyCreator;
    }
}
