import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * FileName  : TestClass.java
 * CreateDate: 2018/7/23 22:54
 *
 * @author Wong
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestClass {
}
