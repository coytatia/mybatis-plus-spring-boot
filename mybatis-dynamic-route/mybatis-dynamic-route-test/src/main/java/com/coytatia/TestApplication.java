package com.coytatia;

import com.coytatia.autoconfigure.EnableDynamicRouteDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * FileName  : TestApplication.java
 * CreateDate: 2018/7/23 23:50
 *
 * @author Wong
 */
@SpringBootApplication
@EnableDynamicRouteDataSource
public class TestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }

}
