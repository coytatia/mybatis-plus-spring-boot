package com.coytatia;

import com.coytatia.db.entity.UserCu;
import com.coytatia.db.mapper.UserCuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Date:   2018/7/6 11:15
 * Author: huangxincheng
 * 请用一句话描述这个类:
 *
 * <author>              <time>            <version>          <desc>
 * huangxincheng     2018/7/6 11:15         1.0.0
 * <p>
 * 春风十里不如你
 **/
@RestController
@RequestMapping("/usercu")
public class UserCuController {
    @Autowired
    private UserCuMapper userCuMapper;

    @RequestMapping("/caimi")
    public List<UserCu> forCaimi() {
        return userCuMapper.selectForMicai();
    }

    @RequestMapping("/chezhu")
    public List<UserCu> forChezhu() {
        return userCuMapper.selectForChezhu();
    }
}
