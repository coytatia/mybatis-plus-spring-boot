package com.coytatia.db.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Date:   2018/7/6 11:06
 * Author: huangxincheng
 * 请用一句话描述这个类:
 *
 * <author>              <time>            <version>          <desc>
 * huangxincheng     2018/7/6 11:06         1.0.0
 * <p>
 * 春风十里不如你
 **/
@Getter
@Setter
public class UserCu {

    private Long id;

    private String name;
}
