package com.coytatia.db.mapper;


import com.coytatia.core.annotation.SwitchDataSource;
import com.coytatia.db.entity.UserCu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Date:   2018/7/6 11:11
 * Author: huangxincheng
 * 请用一句话描述这个类:
 *
 * <author>              <time>            <version>          <desc>
 * huangxincheng     2018/7/6 11:11         1.0.0
 * <p>
 * 春风十里不如你
 **/
@Mapper
@Repository
@SwitchDataSource("micai")
public interface UserCuMapper {

    @Select("select * from user_cu")
    @SwitchDataSource("micai")
    List<UserCu> selectForMicai();

    @Select("select * from user_cu")
    @SwitchDataSource("chezhu")
    List<UserCu> selectForChezhu();

}
