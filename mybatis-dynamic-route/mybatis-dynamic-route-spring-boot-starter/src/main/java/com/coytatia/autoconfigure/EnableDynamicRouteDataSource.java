package com.coytatia.autoconfigure;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * FileName  : EnableQuickRedisCache.java
 * CreateDate: 2018/8/6 13:00
 *
 * @author Wong
 * <p>
 * Modification  History:
 * Date       Author     Version      Discription
 * -----------------------------------------------------------------------------------
 * 2018/8/6   Wong       1.0             1.0
 * <p>
 * Why & What is modified: <修改原因描述>
 * @version V1.0
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(DynamicRouteDataSourceImportSelector.class)
public @interface EnableDynamicRouteDataSource {
}
