package com.coytatia.autoconfigure.properties;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * FileName  : DruidDataSourceProperties.java
 * CreateDate: 2018/7/24 0:11
 *
 * @author Wong
 */
@ConditionalOnClass(DruidDataSource.class)
@ConditionalOnProperty("spring.datasource.dr.druid")
@ConfigurationProperties("spring.datasource.dr")
public class DruidDataSourceProperties<T extends DruidDataSource> {

    private Map<String, DataSourceInfo<T>> druid;

    public void setDruid(Map<String, DataSourceInfo<T>> druid) {
        this.druid = druid;
    }

    public Map<String, DataSourceInfo<T>> getDruid() {
        return druid;
    }
}
