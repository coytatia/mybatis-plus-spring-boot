package com.coytatia.autoconfigure.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * FileName  : BaseDataSourceProperties.java
 * CreateDate: 2018/7/23 23:29
 *
 * @author Wong
 */
@ConfigurationProperties("spring.datasource.dr")
public class BaseDataSourceProperties {

    private String defaultDataSourceKey;

    public String getDefaultDataSourceKey() {
        return defaultDataSourceKey;
    }

    public void setDefaultDataSourceKey(String defaultDataSourceKey) {
        this.defaultDataSourceKey = defaultDataSourceKey;
    }
}
