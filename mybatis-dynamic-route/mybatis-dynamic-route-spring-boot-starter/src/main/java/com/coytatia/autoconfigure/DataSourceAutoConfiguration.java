package com.coytatia.autoconfigure;

import com.alibaba.druid.pool.DruidDataSource;
import com.coytatia.autoconfigure.properties.BaseDataSourceProperties;
import com.coytatia.autoconfigure.properties.DataSourceInfo;
import com.coytatia.autoconfigure.properties.DruidDataSourceProperties;
import com.coytatia.core.annotation.SwitchDataSource;
import com.coytatia.core.bean.RouteDataSource;
import com.coytatia.handle.DynamicRouteDataSourceProxyConfigure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanInstantiationException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import java.util.HashMap;
import java.util.Map;

/**
 * FileName  : DataSourceAutoConfiguration.java
 * CreateDate: 2018/7/23 23:37
 *
 * @author Wong
 */
@Configuration
@ConditionalOnBean(DynamicRouteDataSourceProxyConfigure.class)
@ConditionalOnClass(value = {SwitchDataSource.class, AbstractRoutingDataSource.class})
@ConditionalOnProperty(name = "spring.datasource.type", havingValue = "com.coytatia.core.bean.RouteDataSource")
//@EnableConfigurationProperties(value = {BaseDataSourceProperties.class})
public class DataSourceAutoConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceAutoConfiguration.class);

   /* @EnableConfigurationProperties(value = {DruidDataSourceProperties.class})
    @ConditionalOnClass(DruidDataSource.class)
    static class DruidDataSourceAutoConfiguration extends DataSourceAutoConfiguration {

        *//*@Bean
        public Map<String, DataSourceInfo<DruidDataSource>> druidDataSources(DruidDataSourceProperties<DruidDataSource> druidDataSourceProperties) {
            return druidDataSourceProperties.getDruid();
        }*//*

     *//*@Bean
        public javax.sql.SwitchDataSource dataSource1(DruidDataSourceProperties<DruidDataSource> druidDataSourceProperties) {
            Map<Object,Object> sourceMap = new HashMap<>();
            Map<String, DataSourceInfo<DruidDataSource>> druidDataSources = druidDataSourceProperties.getDruid();
            for (Map.Entry<String,DataSourceInfo<DruidDataSource>> entry : druidDataSources.entrySet()) {
                sourceMap.put(entry.getKey(),entry.getValue().getDataSource());
                logger.info("注入数据源 :" + entry.getKey());
            }
            SwitchDataSource  dataSource =  new SwitchDataSource ();
            dataSource.setTargetDataSources(sourceMap);
            dataSource.setDefaultTargetDataSource(sourceMap.get(druidDataSourceProperties.getDefaultDataSourceKey()));
            logger.info("注入数据源 SUCCESS");
            return dataSource;
        }*//*
    }

    @EnableConfigurationProperties(value = {HikariDataSourceProperties.class})
    @ConditionalOnClass(HikariDataSource.class)
    static class HikariDataSourceAutoConfiguration extends DataSourceAutoConfiguration {

        *//*@Bean
        public Map<String, DataSourceInfo<HikariDataSource>> hikariDataSources(HikariDataSourceProperties<HikariDataSource> hikariDataSourceProperties) {
            return hikariDataSourceProperties.getHikari();
        }*//*

     *//*@Bean
        public javax.sql.SwitchDataSource dataSource2(HikariDataSourceProperties<HikariDataSource> hikariDataSourceProperties) {
            Map<Object,Object> sourceMap = new HashMap<>();
            Map<String, DataSourceInfo<HikariDataSource>> hikariDataSources = hikariDataSourceProperties.getHikari();
            for (Map.Entry<String,DataSourceInfo<HikariDataSource>> entry : hikariDataSources.entrySet()) {
                sourceMap.put(entry.getKey(),entry.getValue().getDataSource());
                logger.info("注入数据源 :" + entry.getKey());
            }
            SwitchDataSource  dataSource =  new SwitchDataSource ();
            dataSource.setTargetDataSources(sourceMap);
            dataSource.setDefaultTargetDataSource(sourceMap.get(hikariDataSourceProperties.getDefaultDataSourceKey()));
            logger.info("注入数据源 SUCCESS");
            return dataSource;
        }*//*
    }*/


    @EnableConfigurationProperties(value = {BaseDataSourceProperties.class, DruidDataSourceProperties.class/*, HikariDataSourceProperties.class*/})
    @ConditionalOnProperty(name = "spring.datasource.dr.default-data-source-key")
    static class RouteDataSourceAutoConfiguration extends DataSourceAutoConfiguration {

        @Bean
        public RouteDataSource dataSource(BaseDataSourceProperties baseDataSourceProperties/*, HikariDataSourceProperties<HikariDataSource> hikariDataSourceProperties*/, DruidDataSourceProperties<DruidDataSource> druidDataSourceProperties
                /*,Map<String, DataSourceInfo<DruidDataSource>> druidDataSources, Map<String, DataSourceInfo<HikariDataSource>> hikariDataSources*/) {
            Map<Object, Object> sourceMap = new HashMap<>();

            //Map<String, HikariDataSourceProperties> beansOfType = applicationContext.getBeansOfType(HikariDataSourceProperties.class);
            if (druidDataSourceProperties.getDruid() != null) {
                for (Map.Entry<String, DataSourceInfo<DruidDataSource>> entry : druidDataSourceProperties.getDruid().entrySet()) {
                    sourceMap.put(entry.getKey(), entry.getValue().getDataSource());
                    logger.info("Inject DataSource in RouteDataSource : '" + entry.getKey() + "'");
                }
            }

            /*if (hikariDataSourceProperties.getHikari() != null) {
                for (Map.Entry<String, DataSourceInfo<HikariDataSource>> entry : hikariDataSourceProperties.getHikari().entrySet()) {
                    sourceMap.put(entry.getKey(), entry.getValue().getDataSource());
                    logger.info("注入数据源 :" + entry.getKey());
                }
            }*/

            Object defaultTargetDataSource = sourceMap.get(baseDataSourceProperties.getDefaultDataSourceKey());
            if (defaultTargetDataSource == null) {
                throw new BeanInstantiationException(RouteDataSource.class, "Cannot find defaultTargetDataSource is '" + baseDataSourceProperties.getDefaultDataSourceKey() + "' in your setting," +
                        "Please check your configuration 'spring.datasource.default-data-source-key' ");
            }
            RouteDataSource routeDataSource = RouteDataSource.initRouteDataSource(sourceMap, defaultTargetDataSource, baseDataSourceProperties.getDefaultDataSourceKey());
            logger.info("Inject Dynamic RouteDataSource successfully");
            return routeDataSource;
        }
    }
}
