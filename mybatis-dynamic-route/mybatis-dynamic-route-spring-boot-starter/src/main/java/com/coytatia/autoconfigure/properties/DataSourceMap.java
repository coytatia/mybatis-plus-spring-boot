package com.coytatia.autoconfigure.properties;

import java.util.LinkedHashMap;

/**
 * FileName  : DataSourceMap.java
 * CreateDate: 2018/7/25 11:26
 *
 * @author Wong
 * <p>
 * Modification  History:
 * Date       Author     Version      Discription
 * -----------------------------------------------------------------------------------
 * 2018/7/25   Wong       1.0             1.0
 * <p>
 * Why & What is modified: <修改原因描述>
 * @version V1.0
 */
public class DataSourceMap<String,DataSourceInfo> extends LinkedHashMap<String,DataSourceInfo> {
}
