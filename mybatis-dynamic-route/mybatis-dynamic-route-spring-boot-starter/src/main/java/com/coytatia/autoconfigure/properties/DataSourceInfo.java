package com.coytatia.autoconfigure.properties;

import javax.sql.DataSource;

/**
 * FileName  : DataSourceInfo.java
 * CreateDate: 2018/7/25 0:34
 *
 * @author Wong
 */
public class DataSourceInfo<T extends DataSource> {

    private Class<T> sourceType;

    private T dataSource;

    public Class<T> getSourceType() {
        return sourceType;
    }

    public void setSourceType(Class<T> sourceType) {
        this.sourceType = sourceType;
    }


    public T getDataSource() {
        return dataSource;
    }

    public void setDataSource(T dataSource) {
        this.dataSource = dataSource;
    }
}
