package com.coytatia.autoconfigure;

import com.coytatia.handle.DynamicRouteDataSourceInterceptor;
import com.coytatia.handle.DynamicRouteDataSourceProxyConfigure;
import com.coytatia.handle.DynamicRouteSwitchHandler;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

/**
 * FileName  : QuickRedisCacheImportSelector.java
 * CreateDate: 2018/8/6 13:02
 *
 * @author Wong
 * <p>
 * Modification  History:
 * Date       Author     Version      Discription
 * -----------------------------------------------------------------------------------
 * 2018/8/6   Wong       1.0             1.0
 * <p>
 * Why & What is modified: <修改原因描述>
 * @version V1.0
 */
public class DynamicRouteDataSourceImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        System.out.println(importingClassMetadata.getAnnotationAttributes(EnableDynamicRouteDataSource.class.getName()));
        return new String[]{DynamicRouteDataSourceInterceptor.class.getName(),DynamicRouteSwitchHandler.class.getName(),DynamicRouteDataSourceProxyConfigure.class.getName()};
    }
}
