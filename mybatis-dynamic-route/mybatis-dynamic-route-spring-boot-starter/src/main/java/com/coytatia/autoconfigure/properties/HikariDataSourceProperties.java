package com.coytatia.autoconfigure.properties;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * FileName  : HikariDataSourceProperties.java
 * CreateDate: 2018/7/25 0:35
 *
 * @author Wong
 */
@ConditionalOnClass(HikariDataSource.class)
@ConditionalOnProperty("spring.datasource.dr.hikari")
@ConfigurationProperties("spring.datasource.dr")
public class HikariDataSourceProperties<T extends HikariDataSource> {

    private DataSourceMap<String,DataSourceInfo<T>> hikari;

    public DataSourceMap<String, DataSourceInfo<T>> getHikari() {
        return hikari;
    }

    public void setHikari(DataSourceMap<String, DataSourceInfo<T>> hikari) {
        this.hikari = hikari;
    }
}
